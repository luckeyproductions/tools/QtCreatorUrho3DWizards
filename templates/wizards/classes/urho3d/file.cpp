%{Cpp:LicenseTemplate}\

#include "%{HdrFileName}"

@if '%{Base}' === 'Drawable'
%{CN}::%{CN}(Context* context, unsigned char drawableFlags): Drawable(context, drawableFlags)
@elsif %{isUrho3DClass}
%{CN}::%{CN}(Context* context): %{Base}(context)
@elsif '%{Base}'
%{CN}::%{CN}(): %{Base}()
@else
%{CN}::%{CN}()
@endif
{
}

@if '%{Base}' === 'Drawable'
void %{CN}::OnSetEnabled() { %{Base}::OnSetEnabled(); }
void %{CN}::ProcessRayQuery(const RayOctreeQuery& query, PODVector<RayQueryResult>& results) { %{Base}::ProcessRayQuery(query, results); }
void %{CN}::Update(const FrameInfo& frame) {}
void %{CN}::UpdateBatches(const FrameInfo& frame) { %{Base}::UpdateBatches(frame); }
void %{CN}::UpdateGeometry(const FrameInfo& frame) {}
UpdateGeometryType %{CN}::GetUpdateGeometryType() { return UPDATE_NONE; }
Geometry* %{CN}::GetLodGeometry(unsigned batchIndex, unsigned level) { return %{Base}::GetLodGeometry(batchIndex, level); }
unsigned %{CN}::GetNumOccluderTriangles() { return 0; }
bool %{CN}::DrawOcclusion(OcclusionBuffer* buffer) { return true; }
void %{CN}::DrawDebugGeometry(DebugRenderer* debug, bool depthTest) { %{Base}::DrawDebugGeometry(debug, depthTest); } // Draw bounding box

void %{CN}::OnNodeSet(Node* node) { Drawable::OnNodeSet(node); }
void %{CN}::OnSceneSet(Scene* scene) { Drawable::OnSceneSet(scene); }
void %{CN}::OnMarkedDirty(Node* node) { Drawable::OnMarkedDirty(node); }
void %{CN}::OnWorldBoundingBoxUpdate() { worldBoundingBox_.Merge(node_->GetWorldPosition()); }
void %{CN}::OnRemoveFromOctree() {}
@elsif '%{Base}' === 'Component' || '%{Base}' === 'LogicComponent'
void %{CN}::OnSetEnabled() { %{Base}::OnSetEnabled(); }
@if '%{Base}' === 'LogicComponent'
void %{CN}::Start() {}
void %{CN}::DelayedStart() {}
void %{CN}::Stop() {}
void %{CN}::Update(float timeStep) {}
void %{CN}::PostUpdate(float timeStep) {}
void %{CN}::FixedUpdate(float timeStep) {}
void %{CN}::FixedPostUpdate(float timeStep) {}
@endif
void %{CN}::OnNodeSet(Node* node) {}
void %{CN}::OnSceneSet(Scene* scene) {}
void %{CN}::OnMarkedDirty(Node* node) {}
void %{CN}::OnNodeSetEnabled(Node* node) {}
bool %{CN}::Save(Serializer& dest) const { return %{Base}::Save(dest); }
bool %{CN}::SaveXML(XMLElement& dest) const { return %{Base}::SaveXML(dest); }
bool %{CN}::SaveJSON(JSONValue& dest) const { return %{Base}::SaveJSON(dest); }
void %{CN}::MarkNetworkUpdate() { %{Base}::MarkNetworkUpdate(); }
void %{CN}::GetDependencyNodes(PODVector<Node*>& dest) {}
void %{CN}::DrawDebugGeometry(DebugRenderer* debug, bool depthTest) {}
@elsif '%{Base}' === 'Serializable'
void %{CN}::OnSetAttribute(const AttributeInfo& attr, const Variant& src) { %{Base}::OnSetAttribute(attr, src); }
void %{CN}::OnGetAttribute(const AttributeInfo& attr, Variant& dest) const { %{Base}::OnGetAttribute(attr, dest); }
const Vector<AttributeInfo>* %{CN}::GetAttributes() const { return %{Base}::GetAttributes(); }
const Vector<AttributeInfo>* %{CN}::GetNetworkAttributes() const { return %{Base}::GetNetworkAttributes(); }
bool %{CN}::Load(Deserializer& source) { return %{Base}::Load(source); }
bool %{CN}::Save(Serializer& dest) const { return %{Base}::Save(dest); }
bool %{CN}::LoadXML(const XMLElement& source) { return %{Base}::LoadXML(source); }
bool %{CN}::LoadJSON(const JSONValue& source) { return %{Base}::LoadJSON(source); }
bool %{CN}::SaveXML(XMLElement& dest) const { return %{Base}::SaveXML(dest); }
bool %{CN}::SaveJSON(JSONValue& dest) const { return %{Base}::SaveJSON(dest); }

void %{CN}::ApplyAttributes() {}
bool %{CN}::SaveDefaultAttributes() const { return false; }
void %{CN}::MarkNetworkUpdate() {}
@elsif '%{Base}' === 'Resource' || '%{Base}' === 'ResourceWithMetadata'
bool %{CN}::BeginLoad(Deserializer& source) { return false; } // This always needs to be overridden by subclasses
bool %{CN}::EndLoad() { return true; } // If no GPU upload step is necessary, no override is necessary

bool %{CN}::Save(Serializer& dest) const
{
    URHO3D_LOGERROR("Save not supported for " + GetTypeName());
    return false;
}

bool %{CN}::SaveFile(const String& fileName) const { return %{Base}::SaveFile(fileName); }
@endif
